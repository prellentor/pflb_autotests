package ru.pflb.autotests.dateConverter;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class testDateConverter {
    private DateConverter dateConverter;

    @BeforeClass
    public void setUp()
    { dateConverter = new DateConverter(); }

    /*
     * 1) dd/MM/YY
     * 2) YYYY-MM-dd
     * 3) dd_MM
     */
    @DataProvider(name = "ValidData")
    public Object[][] dataProvider()
    {   return new Object[][]{
            {"21-10-1993","mode2","1993-10-21"},
            {"29-02-2504","mode1","29/02/25"},
            {"1-01-2020","mode3","1_01"}};
    }

    @Test (dataProvider = "ValidData")
    public void testConvertDateParametrized  (String date, String mode, String expected)
    {
        String result = "";
        try
        { result = dateConverter.convertDate(date, mode); }
        catch (DateConverterExceptions thrown) { }
        Assert.assertEquals(result, expected);
    }


    @DataProvider(name = "InvalidData")
    public Object[][] invalidDataProvider()
    {
        return new Object[][]{
                {"","mode2","Input date cant be empty!"},
                {null,"mode2","Input date cant be empty!"},
                {"122-00-2020","mode2","Input date must be written as <dd-MM-YYYY> pattern!"},
                {"12-020-2020","mode2","Input date must be written as <dd-MM-YYYY> pattern!"},
                {"1-00-2020","mode2","Month number is out of range!"},
                {"1-13-2020","mode2","Month number is out of range!"},
                {"29-02-2019","mode2","Day number is out of range!"},
                {"0-01-2020","mode2","Day number is out of range!"},
                {"32-01-2020","mode2","Day number is out of range!"}
        };
    }

    @Test (dataProvider = "InvalidData") //, expectedExceptions = DateConverterExceptions.class
    public void testConvertDateInvalidParametrized  (String date, String mode, String expected)
    {
        String result;
        try
        { result = dateConverter.convertDate(date, mode); }
        catch (DateConverterExceptions thrown) {
            Assert.assertEquals(thrown.message, expected);
        }
    }
}