package ru.pflb.autotests.dateConverter;

public class DateConverterExceptions extends Exception {
    String message;

    DateConverterExceptions(String str) {
        message = str;
    }

    public String toString() {
        return ("Exception occurred: " + message);
    }
}
