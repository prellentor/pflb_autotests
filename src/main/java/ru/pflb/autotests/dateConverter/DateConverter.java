package ru.pflb.autotests.dateConverter;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class DateConverter {

    /*Разработать приложение, конвертирующее поступающий на вход параметр String (дата в формате dd-MM-YYYY) в форматы:
    dd-MM-YYYY
    1234567890
    * 1) dd/MM/YY
    * 2) YYYY-MM-dd
    * 3) dd_MM
    * Предусмотреть выброс собственного исключения в случае передачи на вход null или параметра, не соответствующего маске dd-MM-YYYY*/

    public String convertDate (String date, String mode) throws DateConverterExceptions {
        Integer[] dateArray = parseString (date);
        String month = "";

        if (dateArray[1] < 10) month = "0" + dateArray[1].toString();
        else month += dateArray[1].toString();

        if (mode == "mode1")
        {
            dateArray[2] /= 100;
            return dateArray[0] + "/" +  month+ "/" + dateArray[2];
        }
        if (mode == "mode2")
            return dateArray[2] + "-" + month + "-" + dateArray[0];
        if (mode == "mode3")
            return dateArray[0] + "_" + month;
        return "";
    }

    static Integer[] daysArray = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    Integer[] parseString(String date) throws DateConverterExceptions {
        if (date == null || date == "")
            throw new DateConverterExceptions("Input date cant be empty!");

        if((Pattern.matches("\\d\\d[-]\\d\\d[-]\\d\\d\\d\\d", date))
                || (Pattern.matches("\\d[-]\\d\\d[-]\\d\\d\\d\\d", date))) ; //mkay
        else throw new DateConverterExceptions("Input date must be written as <dd-MM-YYYY> pattern!");

        String sDays = date.substring(0, date.indexOf('-'));
        Integer days = 0;
        days = Integer.parseInt(sDays);
        date = date.substring(date.indexOf('-') + 1); // - remove <dd-> from string

        String sMonth = date.substring(0, date.indexOf('-'));
        Integer month = 0;
        month = Integer.parseInt(sMonth);
        date = date.substring(date.indexOf('-') + 1); // - remove <mm-> from string

        Integer year = 0;
        year = Integer.parseInt(date);

        if (month > 12 || month < 1)
            throw new DateConverterExceptions("Month number is out of range!");

        if (year % 4 == 0 && month == 2) {
            if ((days > 29) || days < 1)
                throw new DateConverterExceptions("Day number is out of range!");
        }
        else if (days > daysArray[month - 1] || days < 1)
            throw new DateConverterExceptions("Day number is out of range!");

        return new Integer[] { days, month, year };
    }

}
